$('document').ready(function () {

    //One Parent
    $('#oneParent').bind('click submit', function (e) {
        e.preventDefault()

        var regno = $('#reg').val()
        var message = $('#sms').val()

        var data = {
            regno: regno,
            message: message
        }

        axios.post(API + 'single-parent', data).then(function (res) {
            var data = res.data;

            if (data.error) {
                return swal('Oops', data.message, 'error')
            }

            $('#reg').val('')
            $('#sms').val('')

            return swal('Success', "SMS sent to parent", 'success')


        }).catch(function (error) {
            console.log(error)
            swal('Oops', "An error occured. Restart the app", 'error')
        })
    })

    //One Class
    $('#oneClass').bind('click submit', function (e) {
        e.preventDefault()

        var classId = $('#class').val()
        var message = $('#sms').val()

        var data = {
            classId: classId,
            message: message
        }

        axios.post(API + 'single-class', data).then(function (res) {
            var data = res.data;

            if (data.error) {
                return swal('Oops', data.message, 'error')
            }

            $('#class').val('')
            $('#sms').val('')

            return swal('Success', "SMS sent to class " + classId, 'success')


        }).catch(function (error) {
            console.log(error)
            swal('Oops', "An error occured. Restart the app", 'error')
        })
    })

    //Send to all
    $('#all').bind('click submit', function (e) {
        e.preventDefault()

        var message = $('#sms').val()

        var data = {
            message: message
        }

        axios.post(API + 'all', data).then(function (res) {
            var data = res.data;

            if (data.error) {
                return swal('Oops', data.message, 'error')
            }

            $('#sms').val('')

            return swal('Success', "SMS sent to school ", 'success')


        }).catch(function (error) {
            console.log(error)
            swal('Oops', "An error occured. Restart the app", 'error')
        })
    })

    //One Teacher
    $('#staffId').bind('click submit', function (e) {
        e.preventDefault()

        var staffId = $('#staffId').val()
        var message = $('#sms').val()

        var data = {
            staffId: staffId,
            message: message
        }

        axios.post(API + 'single-teacher', data).then(function (res) {
            var data = res.data;

            if (data.error) {
                return swal('Oops', data.message, 'error')
            }

            $('#staffId').val('')
            $('#sms').val('')

            return swal('Success', "SMS sent to staff", 'success')


        }).catch(function (error) {
            console.log(error)
            swal('Oops', "An error occured. Restart the app", 'error')
        })
    })

    //Class teacher
    $('#classTeacher').bind('click submit', function (e) {
        e.preventDefault()

        var groupId = $('#groupId').val()
        var message = $('#sms').val()

        var data = {
            groupId: groupId,
            message: message
        }

        axios.post(API + 'single-group', data).then(function (res) {
            var data = res.data;

            if (data.error) {
                return swal('Oops', data.message, 'error')
            }

            $('#class').val('')
            $('#sms').val('')

            return swal('Success', "SMS sent to group Id " + groupId, 'success')


        }).catch(function (error) {
            console.log(error)
            swal('Oops', "An error occured. Restart the app", 'error')
        })
    })

    //allTeachers
    $('#allTeachers').bind('click submit', function (e) {
        e.preventDefault()

        var message = $('#sms').val()

        var data = {
            message: message
        }

        axios.post(API + 'staff', data).then(function (res) {
            var data = res.data;

            if (data.error) {
                return swal('Oops', data.message, 'error')
            }

            $('#sms').val('')

            return swal('Success', "SMS sent to staff ", 'success')


        }).catch(function (error) {
            console.log(error)
            swal('Oops', "An error occured. Restart the app", 'error')
        })
    })

    //Login
    $('#login').bind('click submit', function (e) {
        e.preventDefault()

        var username = $('#username').val()
        var password = $('#password').val()

        var data = {
            username: username,
            password: password
        }

        axios.post(API + 'auth', data).then(function (res) {
            var data = res.data;

            if (data.error) {
                return swal('Oops', "Invalid Username or Password. Please try again", 'error')
            }

            window.location = "home.html"

        }).catch(function (error) {
            swal('Oops', "An error occured. Restart the app", 'error')
        })
    })

})